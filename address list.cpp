#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#define HUMAN 100

typedef struct 
{
	int x;
	char name[8];
	char num[8];
}MENU;

MENU a[HUMAN];
int people = 0;
void sort();
void add();
void omit();
void modification();
void search();
void printf();

int main() 
{
	while (1)
	{
		int a = 0;
		printf();
		scanf("%d", &a);
		printf("\n");
		switch (a)
		{
		case 1:sort(); break;
		case 2:add(); break;
		case 3:omit(); break;
		case 4:modification(); break;
		case 5:search(); break;
		case 6:return 0; break;
		default:printf("Error!!!\n错误操作指令, 请重新输入");
		system("pause"); break;
		}
		getchar();
		system("cls");
	}
}
void sort() 
{
	if (people == 0)
	{
		printf("无数据可排序");
		system("pause");
		return;
	}
	int flag;
	printf("请选择排序的方式\n1）编号排序  2）姓名排序\n您的输入是：");
	scanf("%d", &flag);
	if (flag!=1&&flag!=2)
	{
		printf("\n输入错误");
		system("pause");
		return;
	}
	MENU b;
	if (flag==1)
	{
		for (size_t i = 0; i < HUMAN; i++)
		{
			for (size_t j = 0; j < HUMAN-1; j++) 
			{
				if (a[j].x>a[j+1].x)
				{
					b = a[j+1];
					a[j + 1] = a[j];
					a[j] = b;
				}
			}
		}
		printf("编号排序成功");
		system("pause");
	}
	if (flag==2)
	{
		for (size_t i = 0; i < HUMAN; i++)
		{
			for (size_t j = 0; j < HUMAN - 1; j++) 
			{
				if (strcmp(a[j].name, a[j + 1].name) > 0)
				{
					b = a[j + 1];
					a[j + 1] = a[j];
					a[j] = b;
				}
			}
		}
		printf("姓名排序成功");
		system("pause");
	}
}
void add() 
{
	if (people>= HUMAN)
	{
		printf("通讯录已满");
		system("pause");
		return;
	}
	int n = 0;
	printf("添加操作：\n请输入添加位置：");
	scanf("%d", &n);
	if (n> HUMAN|| n <=0 )
	{
		printf("处理编号超过阈值");
		system("pause");
		return;
	}
	for (size_t i = 0; i < HUMAN;i++) 
	{
		if (a[i].x==n)
		{
			printf("此处已有数据");
			system("pause");
			return;
		}
	}
	a[people].x = n;
	printf("请输入联系人姓名：");
	scanf("%s", &a[people].name);
	printf("请输入联系人电话：");
	scanf("%s", &a[people].num);
	people++;
}
void omit() 
{
	if (people==0)
	{
		printf("无数据可删除");
		system("pause");
		return;
	}
	int n = 0,i,flag=0;
	printf("删除操作：\n请输入删除位置：");
		scanf("%d", &n);
		if (n > HUMAN || n <= 0)
		{
			printf("处理编号超过阈值");
			system("pause");
			return;
		}
		for (i = 0; i < HUMAN; i++)
		{
			if (a[i].x == n)
			{
				flag = 1;
				break;
			}
		}
		if (flag==0)
		{
			printf("此处无数据");
			system("pause");
			return;
		}
		a[i] = { 0 } ;
		people--;
}
void modification()
{
	if (people == 0)
	{
		printf("无数据可修改");
		system("pause");
		return;
	}
	int n = 0,i,flag=0;
	printf("修改操作：\n请输入修改位置：");
	scanf("%d", &n);
	if (n > HUMAN || n <= 0)
	{
		printf("处理编号超过阈值");
		system("pause");
		return;
	}
	for (i = 0; i < HUMAN; i++)
	{
		if (a[i].x == n)
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0)
	{
		printf("此处无数据");
		system("pause");
		return;
	}
	printf("已擦除原有信息，请重新键入\n");
	printf("请输入联系人姓名：");
	scanf("%s", &a[i].name);
	printf("请输入联系人电话：");
	scanf("%s", &a[i].num);
}
void search() {
	if (people == 0)
	{
		printf("无数据可查");
		system("pause");
		return;
	}
	int flag;
	printf("请选择查询的方式\n1）姓名查询  2）电话查询\n您的输入是：");
	scanf("%d", &flag);
	if (flag != 1 && flag != 2)
	{
		printf("\n输入错误");
		system("pause");
		return;
	}
	if (flag==1)
	{
		char name[11];
		printf("姓名：");
		scanf("%s", &name[0]);
		for (size_t i = 0; i < HUMAN; i++)
		{
			if (strcmp(name,a[i].name)==0)
			{
				printf("编号： %d | 姓名： %s | 电话： %s\n", a[i].x, a[i].name, a[i].num);
				getchar();
				break;
			}
			if (i==HUMAN-1)
			{
				printf("查无此人");
				system("pause");
			}
		}
	}
	if (flag == 2)
	{
		char num[11];
		printf("电话：");
		scanf("%s", &num[0]);
		for (size_t i = 0; i < HUMAN; i++)
		{
			if (strcmp(num, a[i].num) == 0)
			{
				printf("编号： %d | 姓名： %s | 电话： %s\n", a[i].x, a[i].name, a[i].num);
				getchar();
				break;
			}
			if (i == HUMAN - 1)
			{
				printf("查无此人");
				system("pause");
			}
		}
	}
}
void printf() 
{
	printf("=========通讯录==========\n\n\n");
	printf("==========界面==========\n");
	printf("人数：    %d 人\t\t| 剩余空间：     %d 人\n",people, HUMAN-people);
	for (size_t i = 0; i < HUMAN; i++)
	{
		if (a[i].x!=0)
		printf("编号： %d | 姓名： %s | 电话： %s\n",a[i].x,a[i].name,a[i].num);
	}
	printf("\n\n操作列表：\n");
	printf("1)排序\t2)添加\t3)删除\n4)修改\t5)查找\t6)退出程序\n");
	printf("请输入操作：");
}
