#include <stdio.h>
#include <stdlib.h>
#include<time.h>
void Frosh();
void Sophomore();
void Junior();
void Help();
void Error();
void menu();
int main()
{
	int n;
	printf("==========口算生成器==========\n"); 
	printf("\n");
	printf("\n");
	printf("欢迎使用口算生成器 :\n");
	Help();
	printf("\n");
	printf("\n");
	menu();
	scanf("%d", &n);
	while (n!=5)
	{
		printf("<执行操作:)\n");
		printf("\n");
		printf("\n");
		switch(n)
		{
			case 1:Frosh(); break;
			case 2:Sophomore(); break;
			case 3:Junior(); break;
		    case 4:Help(); break;
		    default:Error(); break;
		}
	    printf("\n");
	    printf("\n");
	    menu();
	    scanf("%d", &n);
    }
    printf("<执行操作:)\n");
    printf("\n");
    printf("\n");
    printf("程序结束，欢迎下次使用\n");
    printf("任意键结束……\n");
    return 0;
}
void Frosh()
{
	int i,n,a,b,c;
	char ch;
	srand((unsigned) time(NULL));
	printf("现在是一年级题目:\n请输入生成个数");
	scanf("%d",&n);
	printf("<执行操作:)\n");
	printf("\n");
	for( i = 0 ; i < n ; i++ )
	{
		a=rand() % 10;
		b=rand() % 10;
		c=rand() % 2;
		if(c==0)
		{
			ch='+';
		}
		else
		{
			ch='-';
		}
		printf("%d %c %d = _\n",a,ch,b);
	} 
}
void Sophomore()
{
	int i,n,a,b,c;
	char ch;
	srand((unsigned) time(NULL));
	printf("现在是二年级题目:\n请输入生成个数");
	scanf("%d",&n);
	printf("<执行操作:)\n");
	printf("\n");
	for( i = 0 ; i < n ; i++ )
	{
		a=rand() % 100;
		b=rand() % 100;
		c=rand() % 2;
		if(c==0&&b!=0)
		{
			ch='/';
		}
		else
		{
			ch='*';
		}
		printf("%2d %c %2d =_\n",a,ch,b);
	} 
}
void Junior()
{
	int i,n,a,b,c,p,d;
	char ch,op;
	srand((unsigned) time(NULL));
	printf("现在是三年级题目生成个数:\n请输入生成个数");
	scanf("%d",&n);
	printf("<执行操作:)\n");
	printf("\n");
	for( i = 0 ; i < n ; i++ )
	{
		a=rand() % 100;
		b=rand() % 100;
		p=rand() % 100;
		d=rand() % 4;
		c=rand() % 4;
		if(c==0)
		{
			ch='*';
		}
		else if(c==1&&b!=0)
		{
			ch='/';
		}
		    else if(c==2)
		         {
		         	ch='+';
				 }
				  else
				  {
				  	ch='-';
				  }
		if(d==0)
		{
			op='*';
		}
		else if (d==1&&p!=0)
		     {
		     	op='/';
			 }
			 else if(d==2)
			      {
			      	op='+';
				  }
				  else
				  {
				  	op='-';
				  }
		printf("%2d %c %2d %c %2d= _\n",a,ch,b,op,p);
    } 
}
void Help()
{
	printf("帮助信息\n");
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级题目为不超过十位的加减法;\n");
	printf("二年级题目为不超过百位的乘除法;\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
} 
void Error()
{
	printf("Error!!!\n");
	printf("错误操作指令，请重新输\n");
}
void menu()
{
	printf("操作列表:\n");
	printf("1)一年级     2）二年级     3）三年级\n");
	printf("4)帮助     5)退出程序\n");
	printf("请输入操作> ");
}


